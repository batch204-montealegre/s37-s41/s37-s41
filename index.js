const express = require("express");
const mongoose = require ("mongoose");
const cors = require ("cors");
const port = 4000;
const app = express();

const userRoutes = require("./routes/userRoutes");

//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://rmontealegre:admin123@cluster0.cuby1ca.mongodb.net/S37-s41?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;
	db.on("error", console.error.bind(console, "Connectionn Error"));
	db.once("open", () => console.log(`Now Connected to MongoDB Atlas!`));

app.use(cors());
app.use(express.json());
app.use("/users", userRoutes) 
app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})