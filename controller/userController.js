const User = require ("../models/User");
const bcrypt = require("bcrypt");
const auth = require ("../auth");

//Check if email exist
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		if (result.length>0){
			return true
		}
		else{
			return false
		}

		})}

module.exports.registerUser = (reqBody) => {
	let newUser = new User
	({
		firstName :reqBody.firstName,
		lastName :reqBody.lastName,
		email : reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync (<dataToBeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) =>{
		if (error){
			return false
		}

		else{
			return true
		}
	})
}

//User Authentication

module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            //bcrypt.compareSync(<dataTobeCompare>, <dataFromDB>)

            if(isPasswordCorrect) {
                
                return { access: auth.createAccessToken(result)}
            } else {

                return false
            }
        }

    })
}

module.exports.getUserDetails = (reqBody) => {

	console.log(reqBody)
	return User.findById(reqBody.id).then(result => {
		result.password = "";
		return result
	})
}