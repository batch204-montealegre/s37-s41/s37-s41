//this is where endpoints are saved

const express = require ("express");
const router = express.Router();
const userController = require("../controller/userController")
module.exports = router;

//Route for checking if the user's email is already exist in the database
router.post('/checkEmail', (req,res) =>{
	userController.checkEmailExists(req.body).then(resultFromController =>res.send(resultFromController));
});

//Route for User Registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

//User auth

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/details", (req, res) => {
	userController.getUserDetails(req.body).then(resultFromController => res.send(resultFromController));

});

